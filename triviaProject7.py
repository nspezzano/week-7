import question
player1 = input("Please type in the name of player 1. ")
player2 = input("Please type in the name of player 2. ")

def main():
    player1_points = 0
    player2_points = 0
    player = ''

    questions = get_questions()

    for x in range(10):

        if x % 2 == 0:
            player = player1
        else:
            player = player2
            
        print('Question for', player)


        currentQuestion = questions[x]
        print(currentQuestion)

        inpt = int(input("Which number is correct? "))
        
        if currentQuestion.isCorrect(inpt):
             if player1 == player1:
                 player1_points += 1
             else:
                 player2_points += 1
             print("That is the correct answer.")
             print("")

        else:
             print("That answer is incorrect. The correct answer is", currentQuestion.get_solution())
             player2_points += 1
             print("")
                   
    print(player1, 'earned', player1_points, 'points.')
    print(player2, 'earned', player2_points, 'points.')
    if player1_points == player2_points:
        print('It is a tie.')
    elif player1_points > player2_points:
        print(player1, 'wins the game.')
    else:
        print(player2, 'wins the game.')

def get_questions():

    questions = []


    question1 = question.Question("Which ocean is the smallest?", \
                                   "1.)Arctic Ocean", \
                                   "2.)Pacific Ocean", "3.)Indian Ocean", \
                                   "Atlantic Ocean", 1)
    questions.append(question1)
    question2 = question.Question("In what country was Christmas once illegal?", \
                                  "1.)Russia", "1.)Russia", "3.)England", "4.)Brazil", 3)
    questions.append(question2)
    question3 = question.Question("How many dimples are there on a regular golf ball?" , \
                                  "1.)377", "2.)418", "3.)336", "4.)294", 3)
    questions.append(question3)
    question4 = question.Question("What are the odds of being killed by space debris?", \
                                  "1.)1 in 5 billion",  "2.)1 in 10 billion", "3.)1 in 1 trillion", "4.)1 in 5 million", 1)
    questions.append(question4)
    question5 = question.Question( "How many punds of pressure do you need to rip off your ear?" , \
                                  "1.)7", "2.)2", "3.)17", "4.)11", 1)
    questions.append(question5)
    question6 = question.Question("How much does the earth weigh?", \
                                  "1.)10 x 10^24 tons", "2.)10 x 10^11 tons", "3.)268.77 x 10^38 tons", "4.)65.88 x 10^26 tons", 4)
    questions.append(question6)
    question7 = question.Question( "The 100 years war was fought between what two countries?" , \
                                  "1.)England and Spain", "2.)France and Spain", "3.)France and England", "4.)England and Belgium", 3)
    questions.append(question7)
    question8 = question.Question("Who was the main author of the Declaration of Independance?", \
                                  "1.)Ben Franklin", "2.)Thomas Jefferson", "3.)John Hancock", "4.)George Washington", 2)
    questions.append(question8)
    question9 = question.Question("In what ocean did the Titanic sink?", \
                                  "1.)North Atlantic", \
                                  "2.)North Pacific", "3.)West Atlantic", \
                                  "4.)South Pacific", 1)
    questions.append(question9)
    question10 = question.Question("Approximately how much of the world population died due to the Black Plague?", \
                                   "1.)25%", \
                                   "2.)40%", \
                                   "3.)33%", \
                                   "4.)20%", 3)
    questions.append(question10)
    return questions


main()

    
